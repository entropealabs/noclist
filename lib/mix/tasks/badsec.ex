defmodule Mix.Tasks.Badsec do
  @moduledoc false
  use Mix.Task

  alias NOCList.Badsec

  @shortdoc "Get users from a badsec endpoint"
  def run([url]) do
    Application.ensure_all_started(:noclist)

    url
    |> get_token()
    |> get_users()
    |> report_results()
    |> do_exit()
  end

  def get_token(url) do
    {url, Badsec.auth(url)}
  end

  def get_users({url, {:ok, token}}) when is_binary(token) do
    Badsec.users(url, token)
  end

  def get_users({_url, er}) do
    er
  end

  def report_results({:ok, users}) when is_list(users) do
    Mix.Shell.IO.info(Jason.encode!(users))
  end

  def report_results(er) do
    er
  end

  def do_exit(:ok), do: exit({:shutdown, 1})
  def do_exit(_), do: exit({:shutdown, 0})
end
