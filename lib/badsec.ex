defmodule NOCList.Badsec do
  @auth_token_header "badsec-authentication-token"
  @request_checksum_header "x-request-checksum"

  require Logger

  def auth(url) do
    url
    |> do_request("auth", nil, 3)
    |> parse_response(:auth)
  end

  def users(url, token) do
    url
    |> do_request("users", token, 3)
    |> parse_response(:users)
  end

  def parse_response({:ok, %Mojito.Response{headers: headers}}, :auth),
    do: {:ok, Mojito.Headers.get(headers, @auth_token_header)}

  def parse_response({:ok, %Mojito.Response{body: body}}, :users), do: {:ok, parse_users(body)}

  def parse_response(error, _), do: error

  def parse_users(data), do: String.split(data, "\n")

  def do_request(_url, _path, _token, 0), do: :error_too_many_retries

  def do_request(url, path, token, retries) do
    case call(url, path, token) do
      :error -> do_request(url, path, token, retries - 1)
      data -> data
    end
  end

  def call(url, path, token) do
    headers = maybe_inject_token(token, path)

    case Mojito.request(:get, "#{url}/#{path}", headers) do
      {:ok, %Mojito.Response{status_code: 200}} = resp ->
        resp

      _ ->
        :error
    end
  end

  def maybe_inject_token(nil, _), do: []

  def maybe_inject_token(token, path) when is_binary(token) do
    val =
      :sha256
      |> :crypto.hash("#{token}/#{path}")
      |> Base.encode16()

    [{@request_checksum_header, val}]
  end

  def maybe_inject_token(_, _), do: []
end
