# NOCList

A quick excercise from https://homework.adhoc.team/noclist/

Elixir is definitely a little overkill for a CLI tool. However, this could be moved to a scalable distributed Elixir system and run there.

Requirements:

Make sure you have the asdf-vm plugin manager installed, directions are here https://asdf-vm.com/#/core-manage-asdf-vm


From the project root

1. asdf install (Erlang takes a bit to install, grab a cuppa)
2. mix deps.get
3. mix compile
4. mix badsec http://localhost:8888
